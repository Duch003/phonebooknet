﻿using Microsoft.Extensions.Configuration;
using PhoneBookGo.Data.Models;
using PhoneBookGo.IO.Common;
using PhoneBookGo.IO.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace PhoneBookGo.IO.Writers
{
    public class DatabaseWriter : BaseConnector, IDatabaseWriter
    {
        public DatabaseWriter(IConfiguration configReader) : base(configReader, "ConnectionString")
        {

        }

        public Result InsertPerson(Person newPerson)
        {
            Result output = null;
            var query = "INSERT INTO PhoneBookGo.dbo.People (Name, LastName, Phone, Email) " +
                "VALUES (@placeholderName, @placeholderLastName, @placeholderPhone, @placeholderEmail)";
            var command = new SqlCommand(query, _connection);
            command.Parameters.Add(AddParameter("@placeholderName", newPerson.Name, ResolveType(newPerson.Name.GetType())));
            command.Parameters.Add(AddParameter("@placeholderLastName", newPerson.Name, ResolveType(newPerson.LastName.GetType())));
            command.Parameters.Add(AddParameter("@placeholderPhone", newPerson.Name, ResolveType(newPerson.Phone.GetType())));
            command.Parameters.Add(AddParameter("@placeholderEmail", newPerson.Name, ResolveType(newPerson.Email.GetType())));

            try
            {
                _connection.Open();
                command.Transaction = _connection.BeginTransaction();
                command.ExecuteNonQuery();
                command.Transaction.Commit();

                output = new Result();
            }
            catch (Exception e)
            {
                command.Transaction.Rollback();
                output = new Result(e);
            }
            finally
            {
                _connection.Close();
            }

            return output;
        }

        public Result UpdatePerson(Person newPerson)
        {
            Result output = null;
            var query = "UPDATE PhoneBookGo.dbo.People " +
                "SET Name = @placeholderName," +
                "LastName = @placeholderLastName," +
                "Phone = @placeholderPhone," +
                "Email = @placeholderEmail," +
                "Modified = GETDATE() " +
                "WHERE ID = @placeholderId";
            var command = new SqlCommand(query, _connection);
            command.Parameters.Add(AddParameter("@placeholderId", newPerson.Name, ResolveType(newPerson.ID.GetType())));
            command.Parameters.Add(AddParameter("@placeholderName", newPerson.Name, ResolveType(newPerson.Name.GetType())));
            command.Parameters.Add(AddParameter("@placeholderLastName", newPerson.Name, ResolveType(newPerson.LastName.GetType())));
            command.Parameters.Add(AddParameter("@placeholderPhone", newPerson.Name, ResolveType(newPerson.Phone.GetType())));
            command.Parameters.Add(AddParameter("@placeholderEmail", newPerson.Name, ResolveType(newPerson.Email.GetType())));

            try
            {
                _connection.Open();
                command.Transaction = _connection.BeginTransaction();
                command.ExecuteNonQuery();
                command.Transaction.Commit();

                output = new Result();
            }
            catch (Exception e)
            {
                command.Transaction.Rollback();
                output = new Result(e);
            }
            finally
            {
                _connection.Close();
            }

            return output;
        }

        public Result DeletePerson(int id)
        {
            Result output = null;
            var query = "DELETE FROM PhoneBookGo.dbo.People " +
                "WHERE ID = @placeholderId";
            var command = new SqlCommand(query, _connection);
            command.Parameters.Add(AddParameter("@placeholderId", id, ResolveType(id.GetType())));

            try
            {
                _connection.Open();
                command.Transaction = _connection.BeginTransaction();
                command.ExecuteNonQuery();
                command.Transaction.Commit();

                output = new Result();
            }
            catch (Exception e)
            {
                command.Transaction.Rollback();
                output = new Result(e);
            }
            finally
            {
                _connection.Close();
            }

            return output;
        }
    }
}
