﻿namespace PhoneBookGo.IO.Interfaces
{
    public interface IFileReader
    {
        string ReadAllText(string filename, string filepath);
    }
}