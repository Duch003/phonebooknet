﻿using PhoneBookGo.Data.Models;

namespace PhoneBookGo.IO.Interfaces
{
    public interface IDatabaseWriter
    {
        Result DeletePerson(int id);
        Result InsertPerson(Person person);
        Result UpdatePerson(Person person);
    }
}