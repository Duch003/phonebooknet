﻿using System.Collections.Generic;
using PhoneBookGo.Data.Models;

namespace PhoneBookGo.IO.Interfaces
{
    public interface IDatabaseReader
    {
        Result<List<Person>> SelectPeople();
        Result<Person> SelectPerson(int id);
        Result<List<Person>> SelectPeople(string pattern);
    }
}