﻿using PhoneBookGo.IO.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace PhoneBookGo.IO.Readers
{
    public class FileReader : IFileReader
    {
        public string ReadAllText(string filename, string filepath)
        {
            var fullpath = Path.Combine(filename, filepath);
            var output = "";
            try
            {
                output = File.ReadAllText(fullpath);
            }
            catch (ArgumentException)
            {
                throw;
            }
            catch (IOException) //FileNotFoundException, DirectoryNotFoundException, PathTooLongException
            {
                throw;
            }
            catch (UnauthorizedAccessException e)
            {
                var exception = new IOException($"An unknown error occured while accessing the file <<{filename}>>. Unauthorized access.", e);
                throw exception;
            }
            catch (NotSupportedException e)
            {
                var exception = new IOException($"An unknown error occured while accessing the file <<{filename}>>. NotSupported.", e);
                throw exception;
            }
            catch (Exception)
            {
                throw;
            }

            return output;
        }
    }
}
