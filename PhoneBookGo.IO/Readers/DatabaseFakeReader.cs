﻿using PhoneBookGo.Data.Models;
using PhoneBookGo.IO.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PhoneBookGo.IO.Readers
{
    public class DatabaseFakeReader : IDatabaseReader, IDatabaseWriter
    {
        private static List<Person> _fakeDatabase;
        public DatabaseFakeReader()
        {
            _fakeDatabase = new List<Person>();
            Populate();
        }

        public Result DeletePerson(int id)
        {
            var person = _fakeDatabase.FirstOrDefault(item => item.ID == id);

            if(person != null) { _fakeDatabase.Remove(person); }

            return new Result();
        }
        public Result InsertPerson(Person person)
        {
            person.Created = DateTime.Now;
            if(_fakeDatabase.Count == 0)
            {
                person.ID = 1;
            }
            else
            {
                person.ID = _fakeDatabase.Max(item => item.ID) + 1;
            }
            
            _fakeDatabase.Add(person);

            return new Result();
        }

        public Result<Person> SelectPerson(int id)
        {
            var person = _fakeDatabase.FirstOrDefault(item => item.ID == id);
            return new Result<Person>(person);
        }

        public Result<List<Person>> SelectPeople()
        {
            return new Result<List<Person>>(_fakeDatabase.ToList());
        }

        public Result<List<Person>> SelectPeople(string pattern)
        {
            var people = _fakeDatabase
                .Where(item =>
                item.LastName.Contains(pattern)
                || item.Name.Contains(pattern)
                || item.Phone.Contains(pattern)
                || item.Email.Contains(pattern))
                .ToList();
            return new Result<List<Person>>(people);
        }

        public Result UpdatePerson(Person newPerson)
        {
            var oldPerson = _fakeDatabase.FirstOrDefault(item => item.ID == newPerson.ID);
            oldPerson.Name = newPerson.Name;
            oldPerson.LastName = newPerson.LastName;
            oldPerson.Email = newPerson.Email;
            oldPerson.Phone = newPerson.Phone;
            oldPerson.Modified = DateTime.Now;

            return new Result();
        }

        private void Populate()
        {
            if (_fakeDatabase.Any()) { return; }

            var person = new Person();
            person.ID = 0;
            person.Created = new DateTime(1993, 07, 2);
            person.Modified = new DateTime(2019, 08, 19);
            person.Name = "Adam";
            person.LastName = "FromGod";
            person.Phone = "123456789";
            person.Email = "Adam@HeavenMail.ru";
            _fakeDatabase.Add(person);

            person = new Person();
            person.ID = 1;
            person.Created = new DateTime(1993, 10, 11);
            person.Modified = null;
            person.Name = "Eve";
            person.LastName = "FromAdam";
            person.Phone = "987654321";
            person.Email = null;
            _fakeDatabase.Add(person);
        }
    }
}
