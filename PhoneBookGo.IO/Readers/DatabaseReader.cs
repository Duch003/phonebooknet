﻿using Microsoft.Extensions.Configuration;
using PhoneBookGo.Data.Models;
using PhoneBookGo.IO.Common;
using PhoneBookGo.IO.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace PhoneBookGo.IO.Readers
{
    public class DatabaseReader : BaseConnector, IDatabaseReader
    {
        public DatabaseReader(IConfiguration config) : base(config, "ConnectionString")
        {

        }

        public Result<List<Person>> SelectPeople()
        {
            Result<List<Person>> output = null;
            var list = new List<Person>();
            var query = "SELECT ID, FirstName, LastName, Phone, Email, Created, Modified FROM PhoneBookGo.dbo.People";
            var command = new SqlCommand(query, _connection);

            try
            {
                _connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var newPerson = new Person();
                        newPerson.ID = reader.GetInt32(0);
                        newPerson.Name = reader.GetString(1);
                        newPerson.LastName = reader.GetString(2);
                        newPerson.Phone = reader.GetString(3);
                        newPerson.Email = reader.GetString(4);
                        newPerson.Created = reader.GetDateTime(5);
                        newPerson.Modified = reader.IsDBNull(6) ? null : (DateTime?)reader.GetDateTime(6);
                        list.Add(newPerson);
                    }
                }

                output = new Result<List<Person>>(list);
            }
            catch (Exception e)
            {
                output = new Result<List<Person>>(new List<Person>(), e);
            }
            finally
            {
                _connection.Close();
            }

            return output;
        }

        public Result<List<Person>> SelectPeople(string pattern)
        {
            Result<List<Person>> output = null;
            var list = new List<Person>();
            var query = "SELECT " +
                "ID, " +
                "Name, " +
                "LastName, " +
                "Phone, " +
                "Email, " +
                "Created, " +
                "Modified " +
                "FROM PhoneBookGo " +
                "WHERE Name LIKE @pattern || LastName LIKE @pattern || Phone LIKE @pattern|| Email LIKE @pattern";
            var parameter = AddParameter("@pattern", pattern, ResolveType(pattern.GetType()));
            var command = new SqlCommand(query, _connection);
            command.Parameters.Add(parameter);

            try
            {
                _connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (!reader.Read())
                    {
                        var newPerson = new Person();
                        newPerson.ID = reader.GetInt32(0);
                        newPerson.Name = reader.GetString(1);
                        newPerson.LastName = reader.GetString(2);
                        newPerson.Phone = reader.GetString(3);
                        newPerson.Email = reader.GetString(4);
                        newPerson.Created = reader.GetDateTime(5);
                        newPerson.Modified = reader.IsDBNull(6) ? null : (DateTime?)reader.GetDateTime(6);
                        list.Add(newPerson);
                    }
                }

                output = new Result<List<Person>>(list);
            }
            catch (Exception e)
            {
                output = new Result<List<Person>>(new List<Person>(), e);
            }
            finally
            {
                _connection.Close();
            }

            return output;
        }

        public Result<Person> SelectPerson(int id)
        {
            Result<Person> output = null;
            var list = new List<Person>();
            var query = "SELECT " +
                "ID, " +
                "Name, " +
                "LastName, " +
                "Phone, " +
                "Email, " +
                "Created, " +
                "Modified " +
                "FROM PhoneBookGo " +
                "WHERE ID = @placeholderId";
            var parameter = AddParameter("@placeholderId", id, ResolveType(id.GetType()));
            var command = new SqlCommand(query, _connection);
            command.Parameters.Add(parameter);

            try
            {
                Person temp = null;
                _connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (!reader.Read())
                    {
                        temp = new Person();
                        temp.ID = reader.GetInt32(0);
                        temp.Name = reader.GetString(1);
                        temp.LastName = reader.GetString(2);
                        temp.Phone = reader.GetString(3);
                        temp.Email = reader.GetString(4);
                        temp.Created = reader.GetDateTime(5);
                        temp.Modified = reader.IsDBNull(6) ? null : (DateTime?)reader.GetDateTime(6);
                    }
                }

                output = new Result<Person>(temp);
            }
            catch (Exception e)
            {
                output = new Result<Person>(null, e);
            }
            finally
            {
                _connection.Close();
            }

            return output;
        }
    }
}

