﻿using PhoneBookGo.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhoneBookGo.UI.ViewModels
{
    public class IndexViewModel
    {
        public List<Person> People { get; set; }
        public string Pattern { get; set; }
    }
}
