﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PhoneBookGo.Data.Models;
using PhoneBookGo.IO.Interfaces;

namespace PhoneBookGo.UI.Controllers
{
    public class ManagementController : Controller
    {
        private readonly IDatabaseReader _dbReader;
        private readonly IDatabaseWriter _dbWriter;

        public ManagementController(IDatabaseReader dbReader, IDatabaseWriter dbWriter)
        {
            _dbReader = dbReader;
            _dbWriter = dbWriter;
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var result = _dbReader.SelectPerson(id);

            if (!result.IsFine)
            {
                return RedirectToAction("Error", $"System was unable to edit person.");
            }

            return View(result.Output);
        }

        [HttpPost]
        public IActionResult Edit(Person person)
        {
            if (!ModelState.IsValid)
            {
                return View(person);
            }

            var result = _dbWriter.UpdatePerson(person);

            if (result.IsFine)
            {
                return RedirectToAction("Index", "PhoneBook");
            }

            return RedirectToAction("Error", $"System was unable to update person {person.ToString()}.");
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View(new Person());
        }

        [HttpPost]
        public IActionResult Add(Person person)
        {
            if (!ModelState.IsValid)
            {
                return View(person);
            }

            var result = _dbWriter.InsertPerson(person);

            if (result.IsFine)
            {
                return RedirectToAction("Index", "PhoneBook");
            }

            return RedirectToAction("Error", $"System was unable to add new person {person.ToString()}.");
        }

        [HttpGet]
        public IActionResult Remove(int id)
        {

            var result = _dbWriter.DeletePerson(id);

            if (result.IsFine)
            {
                return RedirectToAction("Index", "PhoneBook");
            }

            return RedirectToAction("Error", $"System was unable to delete person.");
        }

        [HttpGet]
        public IActionResult Error(string message)
        {
            return View(message);
        }
    }
}