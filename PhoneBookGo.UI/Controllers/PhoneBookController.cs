﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PhoneBookGo.Data.Models;
using PhoneBookGo.IO.Interfaces;
using PhoneBookGo.UI.ViewModels;

namespace PhoneBookGo.UI.Controllers
{
    public class PhoneBookController : Controller
    {
        [BindProperty(SupportsGet = true)]
        public string SearchString { get; set; }

        private readonly IDatabaseReader _dbReader;

        public PhoneBookController(IDatabaseReader dbReader)
        {
            _dbReader = dbReader;
        }

        [HttpGet]
        public IActionResult Index(int page = 1)
        {
            var people = _dbReader.SelectPeople();
            if (!people.IsFine)
            {
                return RedirectToAction("Error");
            }

            var pages = (int)Math.Ceiling(people.Output.Count() / 3d);

            if (page <= 0) { page = 1; }

            else if(page > pages)
            {
                page = pages;
            }

            var result = people.Output
                .Skip((page - 1) * 3)
                .Take(3)
                .ToList();

            var viewModel = new IndexViewModel();
            viewModel.Pattern = "";
            viewModel.People = result;
            ViewBag.Title = "Main page";
            ViewBag.Pages = pages;
            ViewBag.Page = page;
            return View(viewModel);
        }

        [HttpGet]
        public IActionResult Error(string error)
        {
            return View(error);
        }
    }
}