# README #

This is one of the exam-like projects written while attending CodersLab programming school. PhoneBookNET (originally PhoneBookGo, but I do not want to confuse anyone with Golang) is a ASP.NET Core MVC application which helps user magaing his/her phone book.

### How do I get set up? ###

* Prerequisites: .NET Core 2.2 runtime, Visual Studio, additionally SQL Server with SQL Server Mangement Studio you would like to switch application to use database
* Download ZIP or clone repository,
* Open PhoneBookGo.sln
* Run and enjoy!
* If you would like to have a dedivated database, please open SSMS and run script deploy_database.sql located in UI project. Then, in Startup.cs, register DatabaseReader and DatabaseWriter classes under respective interfaces IDatabaseReader, IDatabaseWriter.

### Additional packages used in the project ###

* [Bootstrap](https://getbootstrap.com/)