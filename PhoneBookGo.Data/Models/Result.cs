﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBookGo.Data.Models
{
    public class Result<T>
    {
        public Result(T output, Exception error = null)
        {
            Error = error;
            Output = output;
            IsFine = error == null;
        }

        public Exception Error { get; private set; }

        public T Output { get; private set; }

        public bool IsFine { get; private set; }
    }

    public class Result
    {
        public Result(Exception error = null)
        {
            Error = error;
            IsFine = error == null;
        }

        public Exception Error { get; private set; }

        public bool IsFine { get; private set; }
    }
}
