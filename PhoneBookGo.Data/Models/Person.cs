﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhoneBookGo.Data.Models
{
    public class Person
    {
        [Required]
        public int ID { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(20)]
        public string Name { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(40)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(15)]
        [RegularExpression(@"^\d{9,}")]
        public string Phone { get; set; }

        [Required]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$")]
        public string Email { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Modified { get; set; }

        public override string ToString()
        {
            return $"{Name} {LastName}";
        }


    }
}
